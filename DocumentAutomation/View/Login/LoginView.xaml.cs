﻿using DocumentAutomation.Api;
using DocumentAutomation.View.Main;
using DocumentAutomation.View.ModalDialog;
using DocumentAutomation.ViewModel.Login;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DocumentAutomation.View.Login
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class LoginView : MetroWindow
    {
        public LoginView()
        {
            InitializeComponent();
            DataContext = new LoginViewModel();
        }


        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            
           if (await (DataContext as LoginViewModel).Authorization(PasswordBox.Password))
            {
                new MainView().Show();
                this.Hide();
            }

        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
          new ResetPassword().ShowDialog();
           
        }
    }
}
