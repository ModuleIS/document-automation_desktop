﻿using DocumentAutomation.Api;
using DocumentAutomation.Controls.Menu;
using DocumentAutomation.Globals;
using DocumentAutomation.View.Documents;
using DocumentAutomation.View.Position;
using DocumentAutomation.View.Users;
using DocumentAutomation.View.Users.Create;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DocumentAutomation.View.Main
{
    /// <summary>
    /// Логика взаимодействия для MainView.xaml
    /// </summary>
    public partial class MainView : MetroWindow
    {
        public string UserName
        {
            get { return Globals.GlobalData.LoggedInUser.FullName; }
        }
        public MainView()
        {
            InitializeComponent();
            Menu.Add(new ItemMenu
            {
                Header = "Документи",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Завдання",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Повідомлення",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Користувачі",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Посади",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Групи доступу",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Типи документів",
            });
            Menu.Add(new ItemMenu
            {
                Header = "Довідка",
            });

            Menu.SelectedItemCallback = item =>
            {
                if (item.Header == "Користувачі")
                {
                    Content.Content = new UsersView((m) =>
                    {
                        Action(m);
                    });
                }
                if (item.Header == "Документи")
                {
                    Content.Content = new DocumentsView();
                }

                if (item.Header == "Посади")
                {
                    Content.Content = new PositionView();
                }
            };

            DataContext = this;
        }


        public void Action(Object content)
        {
            Content.Content = content;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private async void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var result = await DocumentApi.Instance.LogOut(GlobalData.LoggedInUser);
            if (result)
            {
                Environment.Exit(0);
            }
        }
    }
}
