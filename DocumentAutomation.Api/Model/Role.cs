﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Api.Model
{
    public enum Role
    {
        MAIN_ADMIN,
        [Description("Адміністратор")]
        ADMIN,
        [Description("Користувач")]
        USER
    }
}
