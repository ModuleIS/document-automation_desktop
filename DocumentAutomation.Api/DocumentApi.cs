﻿using DocumentAutomation.Api.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Api
{
    public class DocumentApi
    {
        private DocumentApi()
        {
        }

        private static DocumentApi instance = null;
       
        public static DocumentApi Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DocumentApi();
                }
                return instance;
            }
        }

        public string Authorization { get; private set; }

        private readonly string BaseUrl = "http://94.153.212.78:5000/api/";
        public async Task<(bool, string)> LogIn(UserCredentials user)
        {
            try
            {
                var response = await Post("auth/login", user);
                var obj = JObject.Parse(response);
                if (obj["token"] != null)
                {
                    Authorization = obj["token"].ToString();
                    return (true, Authorization);
                }

            }
            catch { }
            return (false, string.Empty);
        }


        public async Task<bool> ResetPassword(string email)
        {
            try
            {
                return (await PUT("password/reset", new
                {
                    email
                })).Contains("true");
            }
            catch
            {
            }
            return false;
        }

        public async Task<bool> LogOut(UserCredentials user)
        {
            Authorization = null;
            return true;
        }


        public async Task<List<Position>> GetPositions()
        {
            try
            {
                var response = await Get("position/all");
                return JsonConvert.DeserializeObject<List<Position>>(response);
            }
            catch { }
            return new List<Position>();
        }

        public async Task<(int totalPages, List<User> currentUsers)> GetUsers(int page, int size, string query = null)
        {
            try
            {

                object data = new object();

                if(query != null)
                {
                    data = new 
                    {
                        query
                    };
                }

                var response = await Post($"user/get/filter/?page={page}&size={size}&sort=firstName,asc", data);
                var obj = JObject.Parse(response);
                return (int.Parse(obj["totalpages"].ToString()), JsonConvert.DeserializeObject<List<User>>(obj["content"].ToString()));
            }
            catch { }
            return (0, new List<User>());
        }

        public async Task<string> Post(string url, object data)
        {
            try
            {
                var httpWebRequest = WebRequest.CreateHttp($"{BaseUrl}{url}");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (!string.IsNullOrEmpty(Authorization))
                    httpWebRequest.Headers[nameof(Authorization)] = Authorization;

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new LowercaseContractResolver()
                };

                using (var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync()))
                {
                    await streamWriter.WriteAsync(JsonConvert.SerializeObject(data, Formatting.Indented, settings));
                    await streamWriter.FlushAsync();
                }

                var httpResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    return await streamReader.ReadToEndAsync();
            }
            catch
            {
                return string.Empty;
            }
        }

        public async Task<string> Get(string url)
        {
            try
            {
                var httpWebRequest = WebRequest.CreateHttp($"{BaseUrl}{url}");
                httpWebRequest.Method = "GET";

                if (!string.IsNullOrEmpty(Authorization))
                    httpWebRequest.Headers[nameof(Authorization)] = Authorization;

                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)await httpWebRequest.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public async Task<string> PUT(string url, object data)
        {
            try
            {
                var httpWebRequest = WebRequest.CreateHttp($"{BaseUrl}{url}");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";
                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (!string.IsNullOrEmpty(Authorization))
                    httpWebRequest.Headers[nameof(Authorization)] = Authorization;

                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new LowercaseContractResolver()
                };

                using (var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync()))
                {
                    await streamWriter.WriteAsync(JsonConvert.SerializeObject(data, Formatting.Indented, settings));
                    await streamWriter.FlushAsync();
                }

                var httpResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    return await streamReader.ReadToEndAsync();
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
