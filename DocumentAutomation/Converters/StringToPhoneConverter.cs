﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DocumentAutomation.Converters
{
    public class StringToPhoneConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;

            string phoneNo = value.ToString().Replace("(", string.Empty).Replace(")", string.Empty).Replace(" ", string.Empty).Replace("-", string.Empty);
            Regex rg = new Regex(@"\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})");
            MatchCollection m = rg.Matches(phoneNo);
            foreach (Match g in m)
            {
                if (g.Groups[0].Value.Length > 0)
                {
                    _ = g.Groups[0].Value;
                }
            }
            //38 (063) 693 26 92
            return Regex.Replace(phoneNo, @"(\d{2})(\d{3})(\d{3})(\d{2})(\d{2})", "$1 ($2) $3 $4 $5");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
