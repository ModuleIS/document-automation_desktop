﻿using DocumentAutomation.Api;
using DocumentAutomation.View.Users.Create;
using DocumentAutomation.ViewModel.Users;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentAutomation.View.Users
{
    public class Phone
    {
        public string Title { get; set; }
        public string Company { get; set; }
        public int Price { get; set; }
    }
    public partial class UsersView : UserControl
    {
        Action<object> function;
        private UsersViewModel Model { get; set; }
        public UsersView(Action<object> function)
        {
            InitializeComponent();
            Model = new UsersViewModel(RoleCombobox, PositionCombobox, StatusCombobox, phonesGrid);
            DataContext = Model;
            Search.SearchBox.TextChanged += SearchBox_TextChanged;
            this.function = function;
        }

        private async void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var search = (e.Source as TextBox).Text;

            var (totalPages, currentUsers) = await DocumentApi.Instance.GetUsers(0, 10, search);

                Model.Users.Clear();
                Model.Users.AddRange(currentUsers);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            function?.Invoke(new CreateUserView(function));
        }
    }
}
