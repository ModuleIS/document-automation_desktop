﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DocumentAutomation.Api.Model
{
    public enum UserStatus
    {

        [Description("Надіслано запрошення")]
        INVITATION_SENT,
        [Description("Онлайн")]
        ACTIVE,
        [Description("Деактивовано")]
        DEACTIVATED,
        [Description("Відновлення паролю")]
        PASSWORD_RESTORE
    }
}
