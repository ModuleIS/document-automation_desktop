﻿using DocumentAutomation.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Model
{
    public class ResetModel : ViewModelBase, IDataErrorInfo
    {
        private string email { get; set; }

        public string Email
        {
            get{ return email; }
            set { email = value; OnPropertyChanged(nameof(Email)); }
        }
        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Email":
                        if (Email == "")
                            return "Обов'язково до заповнення.";
                        break;
                }
                return string.Empty;
            }
        }
        public string Error { get; }
    }
}
