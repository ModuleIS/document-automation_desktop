﻿using DocumentAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentAutomation.Controls.Menu
{
    /// <summary>
    /// Логика взаимодействия для MenuItem.xaml
    /// </summary>
    public partial class UserControlMenuItem : UserControl
    {
        public UserControlMenuItem(ItemMenu Item)
        {
            InitializeComponent();
            DataContext = Item;
        }

        public ItemMenu CurrentItem
        {
            get { return ((ItemMenu)DataContext); }
        }

        public void ActiveMenu()
        {
            MenuHeader.Foreground = Brushes.Black;

            ((ItemMenu)DataContext).State = true;
            border.BorderBrush = (LinearGradientBrush)this.Resources["Gradient"];
        }

        public void DisableMenu()
        {
            MenuHeader.Foreground = Utils.BrushFromHex("#FF898989");

            ((ItemMenu)DataContext).State = false;
            border.BorderBrush = null;
        }


        private void Label_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ((ItemMenu)DataContext).Callback?.Invoke(((ItemMenu)DataContext), this);
        }

        private void border_MouseEnter(object sender, MouseEventArgs e)
        {
            MenuHeader.Foreground = Brushes.Black;
        }

        private void border_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!CurrentItem.State)
            {
                MenuHeader.Foreground = Utils.BrushFromHex("#FF898989");
            }
        }
    }
}
