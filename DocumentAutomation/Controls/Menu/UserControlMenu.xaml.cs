﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentAutomation.Controls.Menu
{
    /// <summary>
    /// Логика взаимодействия для UserControlMenu.xaml
    /// </summary>
    public partial class UserControlMenu : UserControl
    {
        public List<UserControlMenuItem> MenuItems = new List<UserControlMenuItem>();
        public delegate void SelectedItem(ItemMenu item);
        public SelectedItem SelectedItemCallback;

        public UserControlMenu()
        {
            InitializeComponent();
        }

        public void Add(ItemMenu Item)
        {
            var currentItemMenu = new UserControlMenuItem(Item);
            MenuItems.Add(currentItemMenu);
            Menu.Children.Add(currentItemMenu);
            Item.Callback = (item, control) =>
            {
                var activeMenu = MenuItems.FirstOrDefault(z => ((ItemMenu)z.DataContext).State);

                if (activeMenu != null)
                    activeMenu.DisableMenu();

                control.ActiveMenu();

                SelectedItemCallback?.Invoke(item);
            };
        }
    }
}
