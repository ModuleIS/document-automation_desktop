﻿using DocumentAutomation.Api;
using DocumentAutomation.Model;
using MahApps.Metro.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DocumentAutomation.View.ModalDialog
{

    public partial class ResetPassword : MetroWindow
    {
        ResetModel model { get; set; }
        
        public ResetPassword()
        {
            InitializeComponent();
            DataContext = model = new ResetModel();
        }

        private void LabelBack(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        private async void ButtonPaswordReset(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(model.Email))
            {
                btnReset.IsEnabled = false;
                btnReset.Content = "Відправляю...";
                btnReset.Background = Utilities.Utils.BrushFromHex("#F8F8F8");
                if (await DocumentApi.Instance.ResetPassword("kzandernika@gmail.com"))
                {
                    ResetForm.Visibility = Visibility.Collapsed;
                    SuccessForm.Visibility = Visibility.Visible;
                }
            }
        }

        private void ButtonSuccessBack(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
