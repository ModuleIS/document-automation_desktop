﻿using DocumentAutomation.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Globals
{
   public class GlobalData
    {
        public static UserCredentials LoggedInUser { get; set; }
    }
}
