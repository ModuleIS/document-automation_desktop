﻿using DocumentAutomation.Api;
using DocumentAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentAutomation.View.Position
{
    /// <summary>
    /// Логика взаимодействия для PositionView.xaml
    /// </summary>
    public partial class PositionView : UserControl
    {
        public ObservableRangeCollection<Api.Model.Position> Positions { get; set; } = new ObservableRangeCollection<Api.Model.Position>();
        public PositionView()
        {
            InitializeComponent();
            DataContext = this;

            LoadData();
        }

        public async void LoadData()
        {
            var result = await DocumentApi.Instance.GetPositions();
            Positions.AddRange(result);
        }
    }
}
