﻿using DocumentAutomation.Api;
using DocumentAutomation.Api.Model;
using DocumentAutomation.Controls.ComboBox;
using DocumentAutomation.Model;
using DocumentAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DocumentAutomation.ViewModel.Users
{
    public class UsersViewModel : ViewModelBase
    {
        public ComboboxModel RoleModel;
        public ComboboxModel PositionModel;
        public ComboboxModel StatusModel;

        UserControl RoleCombobox;
        UserControl PositionCombobox;
        UserControl StatusCombobox;

        private List<Position> Positions { get; set; }
        public ObservableRangeCollection<User> Users { get; set; } = new ObservableRangeCollection<User>();
        private DataGrid UsersGrid;
        public ICommand ResetButtonCommand { get; set; }

        public UsersViewModel
            (
                UserControl RoleCombobox,
                UserControl PositionCombobox,
                UserControl StatusCombobox,
                DataGrid UsersGrid
            )
        {
            this.RoleCombobox = RoleCombobox;
            this.PositionCombobox = PositionCombobox;
            this.StatusCombobox = StatusCombobox;
            this.UsersGrid = UsersGrid;
            RoleModel = new ComboboxModel(SelectedRole);
            PositionModel = new ComboboxModel(SelectedPosition);
            StatusModel = new ComboboxModel(SelectedStatus);

            Utils.GetDescriptions(typeof(Role)).ToList().ForEach(z =>
            {
                RoleModel.Items.Add(z);
            });
            Utils.GetDescriptions(typeof(UserStatus)).ToList().ForEach(z =>
            {
                StatusModel.Items.Add(z);
            });
            StatusCombobox.DataContext = StatusModel;
            RoleCombobox.DataContext = RoleModel;
            PositionCombobox.DataContext = PositionModel;
            LoadData();

            ResetButtonCommand = new RelayCommand(o => ResetFilters());
        }

        private void ResetFilters()
        {
            (RoleCombobox as UserControlCombobox).Reset();
            (RoleCombobox as UserControlCombobox).Text = "Роль";

            (PositionCombobox as UserControlCombobox).Reset();
            (PositionCombobox as UserControlCombobox).Text = "Посада";

            (StatusCombobox as UserControlCombobox).Reset();
            (StatusCombobox as UserControlCombobox).Text = "Статус";
            UsersGrid.Items.Filter = null;
        }

        public async void LoadData()
        {
            Positions = await DocumentApi.Instance.GetPositions();
            Positions.Select(z => z.Name).ToList().ForEach(z =>
            {
                PositionModel.Items.Add(z);
            });

            var (totalPages, currentUsers) = await DocumentApi.Instance.GetUsers(0, 10);

            if (currentUsers.Any())
            {
                Users.AddRange(currentUsers);
            }
        }

        private void SelectedRole(string data) => UsersGrid.Items.Filter = new Predicate<object>(ComplexFilter);

        private void SelectedPosition(string data) => UsersGrid.Items.Filter = new Predicate<object>(ComplexFilter);

        private void SelectedStatus(string data) => UsersGrid.Items.Filter = new Predicate<object>(ComplexFilter);

        private bool ComplexFilter(object obj)
        {
            var user = obj as User;

            var roleFilter = true;
            var positionFilter = true;
            var statusFilter = true;

            if (RoleModel.SelectedItem != null)
            {
                roleFilter = user.Role.GetDescription() == RoleModel.SelectedItem.ToString();
            }

            if (PositionModel.SelectedItem != null)
            {
                positionFilter = user.Position.Name == PositionModel.SelectedItem.ToString();
            }

            if (StatusModel.SelectedItem != null)
            {
                statusFilter = user.Status.GetDescription() == StatusModel.SelectedItem.ToString();
            }

            return roleFilter && positionFilter && statusFilter;
        }
    }
}
