﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentAutomation.Controls.ComboBox
{
    /// <summary>
    /// Логика взаимодействия для UserControlCombobox.xaml
    /// </summary>
    public partial class UserControlCombobox : UserControl
    {
        private string text;
        public string Text
        {
            get { return Text; }
            set
            {
                text = value;
                CustomCombobox.Text = text;
            }
        }

        public UserControlCombobox()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            CustomCombobox.SelectedIndex = -1;
        }
    }
}
