﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Model
{
    public class ComboboxModel
    {
        public ObservableCollection<object> Items { get; set; } = new ObservableCollection<object>();

        private readonly Action<string> _selectedNotify;
        private object _selecteditem { get; set; }
        public object SelectedItem
        {
            get { return _selecteditem; }
            set { _selecteditem = value; _selectedNotify?.Invoke(value.ToString()); }
        }

        public ComboboxModel(Action<string> SelectedNotify)
        {
            this._selectedNotify = SelectedNotify;
        }
    }
}
