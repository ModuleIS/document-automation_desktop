﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Controls.Menu
{
    public class ItemMenu
    {
        public delegate void _Callback(ItemMenu item, UserControlMenuItem control);

        public _Callback Callback;

        public string Header { get; set; }
        public bool State { get; set; } = false;
    }
}
