﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DocumentAutomation.View.Users.Create
{
    /// <summary>
    /// Логика взаимодействия для CreateUserView.xaml
    /// </summary>
    public partial class CreateUserView : UserControl
    {
        Action<object> function;
        public string Phone { get; set; }
        public CreateUserView(Action<object> function)
        {
            InitializeComponent();
            this.function = function;
            DataContext = this;
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            function?.Invoke(new UsersView(function));
        }
    }
}
