﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.Api.Model
{
    public class UserCredentials
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }
        public string Password { get; set; }

        [JsonIgnore]
        public string OAuthToken { get; set; }

        [JsonIgnore]
        public string FullName
        {
            get
            {
                return $"{SecondName} {FirstName} {Patronymic}";
            }
        }
    }

    public class User : UserCredentials
    {
        public Position Position { get; set; }
        public Role Role { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public UserStatus Status { get; set; }
        public DateTime CreationDate { get; set; }

        public string DateFormat
        {
            get
            {
                return CreationDate.ToString("dd.MM.yyyy");
            }
        }
    }
}
