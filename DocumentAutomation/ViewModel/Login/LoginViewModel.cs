﻿using DocumentAutomation.Api;
using DocumentAutomation.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAutomation.ViewModel.Login
{
    public class LoginViewModel : ViewModelBase
    {
        public UserCredentials User { get; set; }

        public LoginViewModel()
        {
            User = new UserCredentials();
        }
        public async Task<bool> Authorization(string password)
        {
            User.Password = password;
            var (result, OAuthToken) = await DocumentApi.Instance.LogIn(User);
            if (result)
            {
                Globals.GlobalData.LoggedInUser = User;
                Globals.GlobalData.LoggedInUser.OAuthToken = OAuthToken;
            }
            return result;
        }
    }
}
